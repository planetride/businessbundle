<?php

// src/PlanetRide/BusinessBundle/Model/Contact.php

namespace PlanetRide\BusinessBundle\Model;
use PlanetRide\BusinessBundle\Model\Common;
use PlanetRide\BusinessBundle\Model\Opportunity;

/**
 * Description of Contact
 *
 * @author pachi
 */
class Contact {

    public static $SugarModuleName = 'Contacts';
    public $first_name;
    public $last_name;
    private $linked_account;
    public $phone_mobile;
    public $primary_address_street;
    public $primary_address_city;
    public $primary_address_postalcode;
    public $primary_address_country;
    public $email1;
    public $id;
    public $email2;
    public $licence_c;

    /**
     * 
     * @param string $first_name
     * @param string $last_name
     * @param string $phone_mobile
     * @param string $email
     * @param string $city_name
     * @param string $country_name
     * @param string $licence_c
     */
    public function __construct() {
        
    }

    /**
     * Return a CRM contact based on is CRM id
     * @param string id
     * @return Contact
     */
    public static function getContactbyId($id) {


        $response = Sugar::$sugar->get_by_id($id, "Contacts", array("Contacts" => array('id', 'first_name', 'last_name', 'email1')));
        try{
        if (count($response) == 1) {
            if (is_array($response) &&  !array_key_exists('warning', $response[0])) {
                $contact = new Contact(null, null, null, null, null, null, null, null, null, null);
                Sugar::set_propertiesFromArray($contact, $response[0]);
                return $contact;
            } else {
                Common::$logger->error("Contact : getContactbyId => Error retrived an unexpected number of contact with corresponding  id  -> $id");
                return NULL;
            }
        } else {
            Common::$logger->error("User->getContactbyId : Error retrived an unexpected number of contact with corresponding  id :" . $id);
            return NULL;
        }
        }catch (\Exception $exc) {
              Common::$logger->error("Contact:getContactbyId=>Error while getting contact from crm  : crm did not send back valid response" . $exc->getMessage());
              Common::$logger->debug('Contact:getContactbyId=>trying to get this contact from crm ' . $id);
              Common::$logger->debug("Contact:getContactbyId=> Stacktrace : " . $exc->getTraceAsString());
            } 
    }

    public function save() {
        Common::$logger->debug('Contact.save : Name:' . $this->last_name);
        $response = Sugar::$sugar->set(self::$SugarModuleName, Sugar::get_propertiesArray($this));
       if (array_key_exists('id', $response)) {
            $this->id = $response['id'];
        } else {
            throw new \Exception("Error while saving the contact");
        }
    }

    public function deletelinked_account() {
        unset($this->linked_account);
    }

    public function getId()
    {
        return $this->id;
    }
       /**
     * Link an opportunity to an contact in the CRM
     *
     * @param string $opportunityId
     * @return array with nodes created , failed ,deleted.
     */
    public function linkToOpportunity($opportunityId) {
        $response = Sugar::$sugar->set_relationship(self::$SugarModuleName, $this->id, Opportunity::$sugarOpportunityLinkFieldName, $opportunityId);
        return $response;
    }
}
