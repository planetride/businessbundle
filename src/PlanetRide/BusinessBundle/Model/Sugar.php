<?php

// src/PlanetRide/BusinessBundle/Model/Sugar.php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PlanetRide\BusinessBundle\Model;

use PlanetRide\BusinessBundle\Model\Common;
use Asakusuma\SugarWrapper\Rest;

/**
 * Description of Sugar
 *
 * @author pachi
 */
class Sugar {
   /**
     *
     * @var Asakusuma\SugarWrapper\Rest 
     */
    public static $sugar;

    public function __construct($username, $password, $url , $timeout , $logger) {
        self::$sugar = new Rest($logger);
       
        $logger->debug("BusinessBundle : Sugar -> __construct : Creating sugar object with parameter : username= ".$username." url=".$url. "timeout".$timeout);        
        self::$sugar->setUrl($url);
        self::$sugar->setUsername($username);
        self::$sugar->setPassword($password);
        self::$sugar->setTimeout($timeout);     
    }

      public function autoConnect() {
        if (!isset(self::$sugar) || self::$sugar->is_logged_in() == false) {
            Common::$logger->debug("BusinessBundle : Sugar -> autoConnect : Opening a connexion to the CRM");
//            self::$sugar->setUrl($url);
//            self::$sugar->setUsername($username);
//            self::$sugar->setPassword($password);
//            Common::$logger->debug("BusinessBundle:Model:Sugar-> construct : Creating sugar object with parameter : username= ".$username." url=".$url);   
            $noerror = self::$sugar->connect();
            if ($noerror !== true) {
                Common::$logger->error("BusinessBundle : Sugar -> autoConnect : error connection to CRM : ".$noerror);
                throw new \Exception("Unable to connect", $noerror);
            }
        }
    }

    /**
     * Get the value of a specific field in an array
     * The value can be directly inside the array of in the 'value' field of a Sub Array
     * @param array $tb_field Array of fields
     * @param string $fieldName Name of the field in the array
     * @return string
     */
    public static function getFromCRMSoapFormat(array $tb_field, $fieldName) {
        if (array_key_exists($fieldName, $tb_field)) {
            if (is_array($tb_field[$fieldName]) && array_key_exists('value', $tb_field[$fieldName])) {
                return $tb_field[$fieldName]['value'];
            } else {
                return $tb_field[$fieldName];
            }
        }
        return null;
    }

    /**
     * Return object properties in an Array
     * @param type $object
     * @return array
     */
    public static function get_propertiesArray($object) {
        foreach ($object as $key => $value) {
            $propertyarray[$key] = $value;
        }

        return $propertyarray;
    }

    /**
     * 
     * @param type $object
     * @param array $array
     */
    public static function set_propertiesFromArray(&$object, $array) {
        foreach ($array as $key => $value) {
            $object->$key = Sugar::getFromCRMSoapFormat($array, $key);
        }
    }

    public static function objectifythemis($objectname, $relatedobjectname, $results) {
              
        if ($results) {
            if (array_key_exists('entry_list', $results)) {
                $objectList = array();
                $mainclassname = 'PlanetRide\\BusinessBundle\\Model\\' . $objectname;
                $relatedclassname = 'PlanetRide\\BusinessBundle\\Model\\' . $relatedobjectname;

                foreach ($results['entry_list'] as $i => $entry) {
                    $object = new $mainclassname();
                    foreach ($entry['name_value_list'] as $field) {
                            $fieldname = $field['name'];
                            $object->$fieldname = $field['value'];
                    }
                    if (isset($results['relationship_list'][$i])) {
                        foreach ($results['relationship_list'][$i] as $modules) {
                                  //dump($modules);
                            $object->$relatedobjectname = array();
                            //foreach ($modules as $relatedModule) {
                                      //dump($relatedModule);
                                foreach ($modules['records'] as $x => $record) {
                                                //dump($record);
                                    $relatedobject = new $relatedclassname();
                                    foreach ($record as $fields) {
                                             // dump($fields);
                                        //foreach ($fields as $field) {
                                                 // dump($fields);
                                            $fieldname = $fields['name'];
                                                $relatedobject->$fieldname = $fields['value'];
                                       // }
                                    }
                                    array_push($object->$relatedobjectname, $relatedobject);
                                }
                            //}
                        }
                    }
                    //dump($object);
                    array_push($objectList, $object);
                }
                return $objectList;
            } else {
                throw new \Exception("Unable to objectify results, non corresponding array columns");
            }
        } else {
            return array();
        }
    }

        public static function objectify($objectname, $relatedobjectname, $results) {
        if ($results) {
            if (array_key_exists('entry_list', $results)) {
                $objectList = array();
                $mainclassname = 'PlanetRide\\BusinessBundle\\Model\\' . $objectname;
                $relatedclassname = 'PlanetRide\\BusinessBundle\\Model\\' . $relatedobjectname;

                foreach ($results['entry_list'] as $i => $entry) {
                    $object = new $mainclassname();
                    foreach ($entry['name_value_list'] as $field) {
                        $fieldname = $field['name'];
                        $object->$fieldname = html_entity_decode($field['value'], ENT_QUOTES);
                    }
                    if (isset($results['relationship_list'][$i])) {
                        foreach ($results['relationship_list'][$i] as $modules) {
                            $object->$relatedobjectname = array();
                            foreach ($modules as $relatedModule) {
                                foreach ($relatedModule['records'] as $x => $record) {
                                    $relatedobject = new $relatedclassname();
                                    foreach ($record as $fields) {
                                        foreach ($fields as $field) {
                                            $fieldname = $field['name'];
                                            $relatedobject->$fieldname = html_entity_decode($field['value'], ENT_QUOTES);
                                        }
                                    }
                                    array_push($object->$relatedobjectname, $relatedobject);
                                }
                            }
                        }
                    }
                    array_push($objectList, $object);
                }
                return $objectList;
            } else {
                throw new \Exception("Unable to objectify results, non corresponding array columns");
            }
        } else {
            return array();
        }
    }



    

    public static function objectifysimple($objectname, $results) {
        $objectList = array();
        $classname = 'PlanetRide\\BusinessBundle\\Model\\' . $objectname;

        foreach ($results as $entry) {
            $object = new $classname();
            foreach ($entry as $key => $value) {
                $fieldname = $key;
                $object->$fieldname = $value;
            }

            array_push($objectList, $object);
        }
        return $objectList;
    }
    
    public static function objectifysimple2($objectname, $results) {
        $objectList = array();
        $classname = 'PlanetRide\\BusinessBundle\\Model\\' . $objectname;
        
        foreach ($results as $entry) {
            $object = new $classname();
            foreach ($entry['name_value_list'] as $key => $value) {
                $fieldname = $key;
                $object->$fieldname = $value['value'];
            }

            array_push($objectList, $object);
        }
        return $objectList;
    }

}
