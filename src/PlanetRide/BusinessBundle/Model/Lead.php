<?php

// src/PlanetRide/BusinessBundle/Model/Lead.php

namespace PlanetRide\BusinessBundle\Model;

/**
 * Description of Lead
 *
 * @author pachi
 */
class Lead {

    public static $SugarModuleName = 'Leads';
    static $allColumns = array('date_entered', 'first_name', 'last_name', 'phone_mobile', 'email1',
        'primary_address_country', 'cityname_c', 'date_of_tour_start_c', 'date_of_tour_end_c',
        'clientid_c', 'interest_vehicle_c', 'primary_address_postalcode', 'interest_travel_name_c', 
        'travel_price_c', 'interest_destination_web_c', 'interest_travel_formula_c', 'project_advance_c',
        'subscribenewsletter_c', 'assigned_user_name','date_modified','lead_source_description');

public $date_entered;
public $first_name;
public $last_name;
public $phone_mobile;
public $email1;
public $primary_address_country;
public $cityname_c;
public $countryname_c;
public $date_of_tour_start_c;
public $date_of_tour_end_c;
public $clientid_c;
public $pax_traveling_c;      //no. of people travelling
public $travel_description_c;    //rider's massage on quotation model
public $partnerid_c;        // crm partner id should be correspond to worpress partner id 
public $interest_vehicle_c;    //trip vehicle name
public $primary_address_postalcode;
public $interest_travel_name_c;
public $travel_price_c;
public $interest_destination_web_c;
public $interest_travel_formula_c;
public $project_advance_c;
public $subscribenewsletter_c;
public $assigned_user_name;
public $lead_source_description;
public $escort_guide_c;
public $type_of_accomodation_c;
public $parnertravelreference_c;
public $licence_c;
public $account;
public $id;
public $status;


public function __construct() {
        
    }

    /**
     * Return all Lead
     * @param array $columns
     * @param int $limit
     * @param int $offset
     * @param string where filter
     * @return Lead[];
     */
    public static function get($columns, $limit, $offset,$where) {

        $results = Sugar::$sugar->get(
                self::$SugarModuleName, $columns
                , array('limit' => $limit,
            'offset' => $offset,
            'where' => $where,
            'order_by' => 'date_modified DESC')
        );
        return Sugar::objectifysimple("Lead",$results);
    }
    
    
    /**
     * Save the lead in CRM
     * @throws \Exception
     */
    public function save() {
        Common::$logger->info('Lead : save -> trying to save a lead.');
        if (isset($this->id) && !empty($this->id)){
            Common::$logger->debug('Lead : save -> id : ' . $this->id);
        }
        else {
            Common::$logger->debug('Lead : save -> id not found in current lead. : ' . print_r($this,true));
        }
        Common::$logger->info('Lead : save -> calling sugarCRM to get all property of lead.');
        $response = Sugar::$sugar->set(self::$SugarModuleName, Sugar::get_propertiesArray($this));
        if ($response && array_key_exists('id', $response)) {
            Common::$logger->info('Lead : save -> id exist in current lead.');
            $this->id = $response['id'];
        } else {
            Common::$logger->error('Lead : save -> id doesnot exist in current lead. RESPONSE from sugarCRM : ' . print_r($response,true));
            throw new \Exception("Error while saving the lead");
        }
    }
    /**
     * trying to get  all leads which relates to specific emailid from CRM
     * @param  $email
     * @return leads
     */
    public function getLeadByEmailId($email) {
        $email_escaped = strtoupper(addslashes($email));
        $filter_by_email = "
    leads.id IN (
        SELECT eabr_scauth.bean_id
        FROM email_addr_bean_rel AS eabr_scauth
 
        INNER JOIN email_addresses AS ea_scauth
        ON ea_scauth.deleted = 0
        AND eabr_scauth.email_address_id = ea_scauth.id
        AND ea_scauth.email_address_caps = '$email_escaped'
 
        WHERE eabr_scauth.deleted = 0
        AND eabr_scauth.bean_module = 'Leads' 
    )";
        $options = ['limit' => null,
            'offset' => 0,
            'where' => $filter_by_email,
            'order_by' => 'date_modified DESC'];
        //dump($options);
        $response = Sugar::$sugar->get('Leads', array(), $options);
        return Sugar::objectifysimple("Lead", $response);
    }

}
