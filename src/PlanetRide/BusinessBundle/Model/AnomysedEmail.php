<?php

///src/PlanetRide/BusinessBundle/Model/AnomysedEmail.php


namespace PlanetRide\BusinessBundle\Model;



use PlanetRide\BusinessBundle\Model\Common;
use PlanetRide\BusinessBundle\Model\Sugar;
use PlanetRide\BusinessBundle\Model\User;



/**
 * Description of AnomysedEmail
 *
 * @author pachi
 */

class AnomysedEmail {

    public static $SugarModuleName = 'MA_AnomyzedEmail';
    public static $SugarUserAnomysedMailRelation = 'users_ma_anomyzedemail_1';
    public $name;
    public $anomized_email_c;
    public $user;

    public function save() {
        Common::$logger->info('AnomysedEmail -> save : Name:' . $this->name);
        $response = Sugar::$sugar->set(self::$SugarModuleName, Common::get_propertiesArray($this));
        //dump($response);
        if (array_key_exists('id', $response)) {
            $this->id = $response['id'];
        } else
            throw new \Exception("Error while saving the contact");
    }

    public static function getByProperty($property, $value) {
        Common::$logger->info("AnomysedEmail : getByProperty : Trying to find an AnomysedEmail with property : ".$property." = " . $value);
        $options = ['limit' => 10,
            'offset' => 0,
            'where' => $property . ' = "' . $value . '"',
            'order_by' => null];
        //dump($options);
        $response = Sugar::$sugar->get_with_related(AnomysedEmail::$SugarModuleName, array(
            self::$SugarModuleName => array('id', 'name', 'anomized_email_c'),
            self::$SugarUserAnomysedMailRelation => array('id', 'name', 'first_name', 'last_name', 'user_name', 'department', 'id_wp_c', 'walletid_c')
                )
                , $options);
        //dump($response);
        Common::$logger->debug("AnomysedEmail : getByProperty : MailAnomyzer search response : " . print_r($response, true));
        if ($response && array_key_exists('result_count', $response) && $response['result_count'] == 1 && array_key_exists('name_value_list', $response['entry_list'][0])) {
            //$payment = new Payment($response['entry_list'][0]['name_value_list']);
            $AnomysedEmail = new AnomysedEmail();
            Common::set_propertiesFromArray($AnomysedEmail, $response['entry_list'][0]['name_value_list']);
            Common::$logger->info("AnomysedEmail : getByProperty : 1 anomysed email found : " . $AnomysedEmail->name . " anomyzed version is : " . $AnomysedEmail->anomized_email_c.".");
            return Sugar::objectify("AnomysedEmail","User",$response);
    }
    }
    public function getAnomyzedEmail() {
        return $this->anomized_email_c;
    }

    public function setAnomyzedEmail() {
        $i = 1;
        $timestamp = time();
        while (self::getByProperty('anomized_email_c', mail::prefixemailpartner . $timestamp . $i  . "@" . Mail::domainPartner) != null) {
            $i++;
        }
        $this->anomized_email_c = mail::prefixemailpartner . $timestamp . $i  . "@" . Mail::domainPartner;
        $this->save();
        return $this->anomized_email_c;
    }

    public function getRealEmail() {
        return $this->name;
    }

}
