<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PlanetRide\BusinessBundle\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use PlanetRide\BusinessBundle\Model\Common;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionRequestProcessor {

    protected $request_id;
    protected $requestStack;

    public function __construct($requestStack) {
        $this->requestStack = $requestStack;
    }

    public function processRecord(array $record) {
        $request = $this->requestStack->getCurrentRequest();

        if (isset($request->headers) && !is_null($request->headers->get('Request-id')) && ($request->headers->get('Request-id'))) {
            $this->request_id = $request->headers->get('Request-id');
        } else {
            $this->request_id = Common::generate_uuid();
        }


        $record['context']['request-id'] = isset($_SERVER['HTTP_X_REQUEST_ID']) ? $_SERVER['HTTP_X_REQUEST_ID'] : $this->request_id;

        return $record;
    }

}
