<?php

/// src/PlanetRide/BusinessBundle/Model/Common.php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PlanetRide\BusinessBundle\Model;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;
/**
 * Description of Common
 *
 * @author pachi
 */
class Common {

    public static $acccountId;
    public static $token;
    public static $logger;
    public static $request_id;
    private static $userid;
    private static $sms_auth_token;
    private static $sms_account_id;
    private static $activationStatus;
    private static $user;

    /**
     * 
     * @param type $logger
     * @param type $securityContext getting security context from controller of the user
     * Adding request-id inside context ,assign userid into static variable userid
     */
    public static $em;
    
    public function __construct( $em,$acccountId,$token,$logger,$securityContext,$sugar) {
         self::$acccountId =  $acccountId;
         self::$token =  $token;
        self::$em = $em; 
        self::$logger = $logger;
        self::Init($securityContext);
    }
/**
 * 
 * @param type $logger
 * @param type $securityContext getting security context from controller of the user
 * Adding request-id inside context ,assign userid into static variable userid
 */
    public static function Init( $securityContext) {
        self::$logger->debug("BusinessBundleCommon->Init: Start");
        if ($securityContext && $securityContext->getToken()) {
            $user = $securityContext->getToken()->getUser();
            #  self::$logger->debug("BusinessBundleCommon->Init: user : ".   print_r($user,true));
            if (preg_match('/anon/', $user)) {
                self::$user =$user;
                self::$userid = "anonymous";
                self::$sms_account_id =  self::$acccountId;
                self::$sms_auth_token =  self::$token;
             } elseif (in_array('ROLE_API',$user->getRoles())){
                self::$user =$user;
                self::$userid = null;
                self::$sms_account_id =  self::$acccountId;
                self::$sms_auth_token =  self::$token;
            }else{
                self::$user = $user;
                $userid = $user->getid();
                self::$userid = $userid;
                self::$sms_account_id = $user->getpartnerid()->getSmsAccountId();
                self::$sms_auth_token = $user->getpartnerid()->getsms_auth_token();
                self::$activationStatus = $user->getpartnerid()->getActivationStatus();
            }
        }       
    }

    /**
     * creation of new GUID
     * @return type string ID
     */
    public static function generate_uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Return object properties in an Array
     * @param type $object
     * @return array
     */
    public static function get_propertiesArray($object) {
        foreach ($object as $key => $value) {
            if ($value) {
                $propertyarray[$key] = $value;
            }
        }

        return $propertyarray;
    }

    /**
     * 
     * @param type $object
     * @param array $array
     */
    public static function set_propertiesFromArray(&$object, $array) {
        foreach ($array as $key => $value) {
            $object->$key = Sugar::getFromCRMSoapFormat($array, $key);
        }
    }

    public static function getuserid() {
        return self::$userid;
    }

    public function setuserid($userid) {
        self::$userid = $userid;

        return self::$userid;
    }
  # return user with all object
   public static function getuser() {
        return self::$user;
    }
    # sms_auth_token;

    public static function getsms_auth_token() {
        return self::$sms_auth_token;
    }

    public function setsms_auth_token($sms_auth_token) {
        self::$sms_auth_token = $sms_auth_token;

        return self::$sms_auth_token;
    }

    #$sms_account_id

    public static function getsms_account_id() {
        return self::$sms_account_id;
    }

    public function setsms_account_id($sms_account_id) {
        self::$sms_account_id = $sms_account_id;

        return self::$sms_account_id;
    }

    #  activationStatus;

    public static function getactivationStatus() {
        return self::$activationStatus;
    }

    public function setactivationStatus($activationStatus) {
        self::$activationStatus = $activationStatus;

        return self::$activationStatus;
    }

}