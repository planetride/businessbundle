<?php

// src/PlanetRide/BusinessBundle/Model/Payment.php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PlanetRide\BusinessBundle\Model;

/**
 * Description of Payment
 *
 * @author pachi
 */
class Payment {

    public static $SugarModuleName = 'payme_Payments';
    public static $SugarFactureLinkFieldName = 'aos_invoices_payme_payments_1';

    /**
     *
     * @var string Id de la transaction dans le CRM
     */
    public $id;

    /**
     *
     * @var string Id de la transaction chez lemon way
     */
    public $name;

    /**
     *
     * @var string token recu lors du MoneyInWebInit et transmis en parametre à la page de paiement 
     */
    public $paymentToken;

    /**
     *
     * @var string identifiant unique de la transaction généré par Planet Ride lors de la demande de MoneyInWebInit
     *  et recu en paramètre de confirmation après le paiement
     */
    public $wktoken;
    public $status;
    public $cardid;
    public $amount;
    
    /**
     * Invoice bound to the payment
     * @var Invoice 
     */
    public $invoice;
    public $commission;

    /**
     *
     * @var string Descrition of the payment will be sent as comment to LemonWay.
     */
    public $description;
    /**
     * @var string included the Insurance Details such as country category, type of vehicle, no of pax, and lenght of trip.
     */
    public $inclusion_detail_c;
    /**
     *
     * @var type include the insurance amount
     */
    public $inclusion_amount_c;

    /**
     * Constructor
     * 
     */
    public function __construct($paymenttoken, $status, $name, $cardid, $id, $commission, $amount, $wktoken, $description, $insurance_detail, $insurance_amount) {

        $this->paymentToken = $paymenttoken;
        $this->status = $status;
        $this->name = $name;
        $this->cardid = $cardid;
        $this->id = $id;
        $this->commission = $commission;
        $this->amount = $amount;
        $this->wktoken = $wktoken;
        $this->description = $description;
        $this->inclusion_detail_c = $insurance_detail;
        $this->inclusion_amount_c = $insurance_amount;
    }

    /**
     * Save the payment object in the CRM
     * 
     * @param PaymentGateway\Business\Invoice $invoice Related invoice of the payment
     * @return string null if error or payment id if success
     */
    public function save() {
        $values = [];
        if ($this->name) {
            $values['name'] = $this->name;
        }
        if ($this->status) {
            $values['status_c'] = $this->status;
        }
        if ($this->amount) {
            $values['amount_c'] = $this->amount;
        }
        if ($this->cardid) {
            $values['cardid_c'] = $this->cardid;
        }
        if ($this->wktoken) {
            $values['wktoken_c'] = $this->wktoken;
        }
        if ($this->id) {
            $values['id'] = $this->id;
        }
        if ($this->commission) {
            $values['commission_c'] = $this->commission;
        }
        if ($this->description) {
            $values['description'] = $this->description;
        }
        if ($this->inclusion_detail_c) {
            $values['inclusion_detail_c'] = $this->inclusion_detail_c;
        }
        if ($this->inclusion_amount_c) {
            $values['inclusion_amount_c'] = $this->inclusion_amount_c;
        }
        Common::$logger->debug("Payment.save : Request values :" . print_r($values, true));
        $response = Sugar::$sugar->set(self::$SugarModuleName, $values);
        //dump($response);
        Common::$logger->debug("Payment.save : Response :" . print_r($response, true));
        if (array_key_exists('id', $response)) {
            $this->id = $response['id'];
            return $this->id;
        } else {
            throw new \Exception("Error while saving payment in CRM");
        }
    }

    /**
     * Search a Payment in the CRM by wkToken
     * @param string $response_wkToken
     * @return Payment or null if inconsistent data returned
     */
    public static function getPaymentBywkToken($response_wkToken) {
        $options = ['limit' => 500,
            'offset' => 0,
            'where' => "", 'where' => 'wktoken_c = "' . $response_wkToken . '"',
            'order_by' => null];
        Common::$logger->debug("Payment->getPaymentBywkToken : request payment with :" . \print_r($options, true));
        $response = Sugar::$sugar->get_with_related(self::$SugarModuleName, array(
            self::$SugarModuleName => array('id', 'name', 'status_c', 'cardid_c', 'amount_c', 'wktoken_c', 'commission_c', 'description','inclusion_detail_c','inclusion_amount_c'),
            self::$SugarFactureLinkFieldName => array('id', 'name', 'invoice_erp_number_c', 'total_amount', 'status','billing_contact_id','assigned_user_id')
                )
                , $options);
        Common::$logger->debug("Payment->getPaymentBywkToken : response :" . \print_r($response, true));
        if ($response && array_key_exists('result_count', $response) && $response['result_count'] == 1 && array_key_exists('name_value_list', $response['entry_list'][0])
        ) {
            Common::$logger->debug("Payment->getPaymentBywkToken : 1 paiement found :" . \print_r($response['entry_list'][0]['name_value_list'], true));

            //$payment = new Payment($response['entry_list'][0]['name_value_list']);
            $payment = new Payment(null, null, null, null, null, null, null, null, null,null,null);
            Sugar::set_propertiesFromArray($payment, $response['entry_list'][0]['name_value_list']);

        //   dump($response['relationship_list'][0]);
        //   dump($response['relationship_list'][0]['link_list'][0]['records']);
            if (count($response['relationship_list'][0])>0 && array_key_exists('relationship_list', $response) && count($response['relationship_list'][0]['link_list'][0]['records']) == 1) {
                $relatedInvoice = $response['relationship_list'][0]['link_list'][0];
                Common::$logger->debug("Payment->getPaymentBywkToken : 1 related invoice found :" . \print_r($relatedInvoice['records'][0], true));
                //dump($relatedInvoice['records'][0]);
                $invoice = new Invoice(null, null, null, null, null, null, null, null, null, null, null);
                
                Sugar::set_propertiesFromArray($invoice, $relatedInvoice['records'][0]["link_value"]);
                $payment->invoice = $invoice;
            }

            return $payment;
       } else {
            throw new \Exception('Inconsistent number of payment found : ' . count($response));
        }
    }
 
    /**
     * Link a payment to an Invoice in the CRM
     * 
     * @param string $invoiceId
     * @return string Creation message or null if not created
     */
    public function linkToInvoice($invoiceId) {
        $response = Sugar::$sugar->set_relationship(Invoice::$SugarModuleName, $invoiceId, self::$SugarFactureLinkFieldName, $this->id);
        if (\array_key_exists('created', $response)) {
            /* @var $response string */
            return $response['created'];
        } else {
            return null;
        }
    }

}
