<?php
// src/PlanetRide/BusinessBundle/Model/Opportunity.php

namespace PlanetRide\BusinessBundle\Model;

use  PlanetRide\BusinessBundle\Model\Invoice;

/**
 * Description of Opportunity
 *
 * @author pachi
 */
class Opportunity {

    public static $SugarModuleName = 'Opportunities';
    public static $SugarFactureLinkFieldName = 'opportunities_aos_invoices_1';
    static $allColumns = array('assigned_user_name', 'date_closed', 'date_entered', 'sales_stage', 'id');
    public $name;
    public $date_closed;
    public $date_of_tour_start_c;
    public $date_of_tour_end_c;
    public $travel_description_c;      //this is the rider's massage on quotation model.
    public $interest_vehicle_c;     //trip vehicule
    public $interest_travel_name_c;     //the name of the product selected by rider
    public $interest_destination_web_c;  //product destination (name of the trip country)
    public $interest_travel_formula_c;           //travel formula
    public $amount;           //amount of the opportunity
    public $escort_guide_c;   //rider has guided or not
    public $type_of_accomodation_c;        //riders selected hotels etc.
    public $parnertravelreference_c;       //real hidden product id
    public $assigned_user_name;       //will asign to one partner id
    public $date_entered;
    public $sales_stage;
    public $toremember_c;
    public static $sugarOpportunityLinkFieldName="opportunities";
    public function __construct() {
        
    }

    public function getId()
    {
        return $this->id;
    }
    /**
     * Return Opportunities with their related Leads
     * @param array $columns
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getWithRelatedLeads($columns, $leadColumns, $limit, $offset, $where) {

        $results = Sugar::$sugar->get_with_related(self::$SugarModuleName, array(
            self::$SugarModuleName => $columns,
            Lead::$SugarModuleName => $leadColumns,
                ), array(
            'limit' => $limit,
            'offset' => $offset,
            'where' => $where,
            'order_by' => 'date_modified DESC',
                )
        );
        return Sugar::objectify("Opportunity", "Lead", $results);
    }

    /**
     * Return Opportunities with their related Leads
     * @param array $columns
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getWithRelatedContacts($columns, $leadColumns, $limit, $offset, $where) {

        $results = Sugar::$sugar->get_with_related(self::$SugarModuleName, array(
            self::$SugarModuleName => $columns,
            Contact::$SugarModuleName => $leadColumns,
                ), array(
            'limit' => $limit,
            'offset' => $offset,
            'where' => $where,
            'order_by' => 'date_modified DESC',
                )
        );

        return Sugar::objectify("Opportunity","Contact",$results);
                    }
             

    /**
     * Return Opportunities with their related Leads(this function is only made for the cron to work remotely)
     * @param array $columns
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function getWithRelatedContactsCron($columns, $leadColumns, $limit, $offset, $where) {

        $results = Sugar::$sugar->get_with_related(self::$SugarModuleName, array(
            self::$SugarModuleName => $columns,
            Contact::$SugarModuleName => $leadColumns,
                ), array(
            'limit' => $limit,
            'offset' => $offset,
            'where' => $where,
            'order_by' => 'date_modified ASC',
                )
        );

        return Sugar::objectify("Opportunity","Contact",$results);
    }


    /**
     * Return all Opportunities
     * @param array $columns
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function get($columns, $limit, $offset,$where) {

        $results = Sugar::$sugar->get(
                self::$SugarModuleName, $columns
                , array('limit' => $limit,
            'offset' => $offset,
            'where' => $where,
            'order_by' => 'date_modified DESC')
        );
        return Sugar::objectifysimple("Opportunity",$results);
    }

    /**
     * Save the opportunity in CRM
     * @throws \Exception
     */
    public function save() {
        Common::$logger->debug('Opportunity.save');

        $response = Sugar::$sugar->set(self::$SugarModuleName, Sugar::get_propertiesArray($this));
        if (is_array($response) && array_key_exists('id', $response)) {
            $this->id = $response['id'];
        } else {
            throw new \Exception("Error while saving the opportunity");
        }
    }


    public static function getWithRelatedEmail($opportunityid){
        $filter = "opportunities.id  = '"+$opportunityid+"'";
        $results = Sugar::$sugar->get_with_related(Opportunity::$SugarModuleName, array(
            Opportunity::$SugarModuleName => array('id','name'),
            Emails::$SugarModuleName=> array('id','name','from_addr','to_addrs','date_sent','description','description_html') ,
                ), array(
            'limit' => 1,
            'offset' => '',
            'where' => "opportunities.id  = '".$opportunityid."'",
            'order_by' => '',
                )
        );
        //var_dump($results);
        //var_dump(Sugar::objectify( 'Opportunity',Emails::$SugarModuleName ,$results));
        return Sugar::objectify('Opportunity',Emails::$SugarModuleName ,$results);

    }



    public static function getRelatedInvoice($opportunityid) {
        $results = Sugar::$sugar->get_relationship(
                self::$SugarModuleName, //$module_name
                $opportunityid, //$module_id
                'opportunities_aos_invoices_1', //,$link_field_name
                '', //,$related_module_query
                 array('name','number', 'currency_id', 'total_amount', 'commission_c','billing_contact_id','billing_contact','id','status','date_entered'),
                //   array()   , //$related_field1s
                array(), //$related_module_link_name_to_fields_array
                0, //$deleted
                '', //$order_by
                0, //$offset
                0 //$limit
        );
        //dump($results);
        $relatedInvoices = [];
        
        
        if ($results) {
            $relatedInvoices =  Sugar::objectify("Invoice", "Opportunity", $results);
           /*
                    if (array_key_exists('entry_list', $results)) {
                $entryLists = $results['entry_list'];
                foreach ($entryLists as $key => $values) {
                    $invoiceObject = new Invoice();
                $_classMethods = get_class_methods($invoiceObject);
                    $namedValueLists = $values["name_value_list"];
                    foreach ($namedValueLists as $nodename => $associatedValue) {
                        $methodName = 'set_' . $nodename;
                        if (in_array($methodName, $_classMethods)){
                            $invoiceObject->$methodName($associatedValue["value"]);
                        }
                    }
                    $relatedInvoices[] = $invoiceObject;
                }
            }
            * 
            */
        }
        return $relatedInvoices;
    }

    /**
     * Link an opportunity to an Invoice in the CRM
     *
     * @param string $invoiceId
     * @return array with nodes created , failed ,deleted.
     */
    public function linkToInvoice($invoiceId, $opportunityId) {
        $response = Sugar::$sugar->set_relationship(Invoice::$SugarModuleName, $invoiceId, self::$SugarFactureLinkFieldName, $opportunityId);
        return $response;
    }

}
