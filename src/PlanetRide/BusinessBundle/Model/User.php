<?php

/// src/PlanetRide/BusinessBundle/Model/User.php

namespace PlanetRide\BusinessBundle\Model;

use PlanetRide\BusinessBundle\Model\Sugar;
use PlanetRide\BusinessBundle\Model\User;


/**
 * Description of User
 *
 * @author pachi
 */
class User {

    const defaultRequestedColumns =array('id', 'name', 'first_name', 'last_name', 'user_name', 'department', 'id_wp_c', 'walletid_c','immatriculation_number_c','travel_agency_legal_represen_c');
    const SugarModuleName = 'Users';
    public static $SugarUserAnomysedMailRelation = 'users_ma_anomyzedemail_1';
    
    public $id;
    public $walletid_c;
    /*
     * Legal name of the travel agency
     */
    public $department;
    public $last_name;
    public $first_name;
    public $user_name;
    /**
     * Id Wordpress de l'utilisateur
     * @var string 
     */
    public $id_wp_c;
    public $immatriculation_number_c;
    public $travel_agency_legal_represen_c;
    public $address_street;
    public $address_city;
    public $address_state;
    public $address_country;
    public $address_postalcode;

    /**
     * Constructor
     */
    public function __construct() {
       
    }

    /**
     * Return a CRM user based on is CRM id
     * @param string $userId
     * @return User
     */
    public static function getUserbyId($userId,$columns) {
        if (is_null($columns))
        {
            $columns = self::defaultRequestedColumns;
        }

        $response = Sugar::$sugar->get_by_id($userId, self::SugarModuleName, array( self::SugarModuleName => $columns));
        if (count($response) == 1) {
            if (!array_key_exists('warning', $response[0])) {
                return Sugar::objectifysimple('User',$response)[0];
            } else {
                throw new \Exception("User->getUserbyId : Error while search for a user with corresponding  id " . $userId);
            }
        } else {
            throw new \Exception("User->getUserbyId : Error retrivec an unexpected number of User with corresponding  id :" . $userId);
        }
    }

    /**
     * Return a user form CRM based on a Wordress ID
     * @param string $wordpressid
     * @return \PaymentGateway\Business\User
     * @throws Exception
     */
    public static function getUserbyWordpressId($wordpressid) {

        Common::$logger->debug("User->getUserbyWordpressId : request user with wordpress id :" . $wordpressid);
        $options = ['limit' => 500,
            'offset' => 0,
            'where' => "", 'where' => 'id_wp_c = "' . $wordpressid . '"',
            'order_by' => null];
        $response = Sugar::$sugar->get("Users", ['id', 'name', 'first_name', 'last_name', 'user_name', 'department', 'id_wp_c', 'walletid_c'], $options);

        if (count($response) == 1) {
            if (!array_key_exists('warning', $response[0])) {
                $users = Sugar::objectifysimple("User",$response);
                Common::$logger->debug("User->getUserbyWordpressId : user found :" . print_r($users, true));
                //dump($users[0]);
                
                return $users[0];
            } else {
                throw new \Exception("User->getUserbyWordpressId : Error while search for a user with corresponding wordpress id :" . $wordpressid);
            }
        } else {
            throw new Exception("User->getUserbyWordpressId : Error retrivec an unexpected number of User with corresponding wordpress id :" . $wordpressid);
        }
    }
    
    
    /**
     * Return all Users
     * @param array $columns
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public static function get($columns, $limit, $offset,$where) {

        $results = Sugar::$sugar->get(
                self::SugarModuleName, $columns
                , array('limit' => $limit,
            'offset' => $offset,
            'where' => $where,
            'order_by' => 'date_modified DESC')
        );
        return Sugar::objectifysimple("User",$results);
    }
    
       /**
     * Send back 1 or mutliple User with their related Anomyzed email
     * @param type $property
     * @param type $value
     * @return User
     */
    public static function getMultipleByProperty($property, $value) {
        //Common::$logger->info("AnomysedEmail : getByProperty : Trying to find an AnomysedEmail with property : " . $property . " = " . $value);
        $options = ['limit' => 0,
            'offset' => 0,
                'where' => $property. ' = "' . $value . '"',
            'order_by' => null];
        //dump($options);
        $response = Sugar::$sugar->get_with_related('Users', array(
            self::$SugarUserAnomysedMailRelation => array('id', 'name', 'anomized_email_c'),
            'Users' => array('id', 'name', 'first_name', 'last_name', 'user_name', 'department', 'id_wp_c', 'walletid_c')
                )
                , $options);
        //var_dump($response);
        //Common::$logger->debug("AnomysedEmail : getByProperty : MailAnomyzer search response : " . print_r($response, true));
        if ($response && array_key_exists('result_count', $response)  && array_key_exists('name_value_list', $response['entry_list'][0])) {
            //$payment = new Payment($response['entry_list'][0]['name_value_list']);
            $AnomysedEmail = new AnomysedEmail();
            Common::set_propertiesFromArray($AnomysedEmail, $response['entry_list'][0]['name_value_list']);
            //Common::$logger->info("AnomysedEmail : getByProperty : 1 anomysed email found : " . $AnomysedEmail->name . " anomyzed version is : " . $AnomysedEmail->anomized_email_c . ".");
            return Sugar::objectify( "User","AnomysedEmail", $response);
        }
    }
  
     /**
     * Send back 1 string (concatinated email ids)
     * @param type $property
     * @param type $value
     * @return String
     */
    public static function getEmailsById($property, $value) {
        $options = ['limit' => 0,
            'offset' => 0,
                'where' => $property. ' = "' . $value . '"',
            'order_by' => null];
        $response = Sugar::$sugar->get_with_related('Users', array(
            self::$SugarUserAnomysedMailRelation => array('id', 'name', 'anomized_email_c'),
            'Users' => array('id', 'name', 'first_name', 'last_name', 'user_name', 'department', 'id_wp_c', 'walletid_c')
                ), $options);
        if ($response && array_key_exists('result_count', $response)  && array_key_exists('name_value_list', $response['entry_list'][0])) {
            $AnomysedEmail = new AnomysedEmail();
            Common::set_propertiesFromArray($AnomysedEmail, $response['entry_list'][0]['name_value_list']);
            $userInfo =  Sugar::objectify( "User","AnomysedEmail", $response);
            $currentUser = $userInfo[0];
                foreach ($currentUser->AnomysedEmail as $anomyzedEmail) {
                    $arr_emailsFilterValues[] = $anomyzedEmail->anomized_email_c;
                    $arr_emailsFilterValues[] = $anomyzedEmail->name;
                }
                $emailFilterValues = "  " . implode(',', $arr_emailsFilterValues);
                return $emailFilterValues;
        }
    }
    
}
