<?php

// src/PlanetRide/BusinessBundle/Model/Account.php

namespace PlanetRide\BusinessBundle\Model;


use PlanetRide\BusinessBundle\Model\Common;
use PlanetRide\BusinessBundle\Model\Sugar;

/**
 * Description of Account
 *
 * @author pachi
 */
class Account {

    public static $SugarModuleName = 'Accounts';
    
    public $name;
    public $phone_office;
    public $email1;
    public $contacts;
    public $assigned_user_name;
    public $billing_address_country;
    public $billing_address_city;

    /**
     * 
     * @param string $name
     * @param string $phone
     * @param string $email
     * @param array $user
     */
    public function __construct() {

    }
    
    /**
     * Save the accounts in CRM
     * @throws \Exception
     */
    public function save() {  
        Common::$logger->debug('Accounts.save');
        // dump($this);
        $response = Sugar::$sugar->set(self::$SugarModuleName, Sugar::get_propertiesArray($this));
        if ($response && array_key_exists('id', $response)) {
            $this->id = $response['id'];
        } else {
            Common::$logger->error('BusinessBundle : Account -> save : Error while saving the account RESPONSE from SugarCRM : '.print_r($response,true));
            throw new \Exception("Error while saving the account");
        }
    }


}
