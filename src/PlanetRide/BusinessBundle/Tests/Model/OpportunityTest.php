<?php

namespace PlanetRide\BusinessBundle\Tests\Model;

use PlanetRide\BusinessBundle\Model\Opportunity;
use PlanetRide\BusinessBundle\Model\Sugar;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class OpportunityTest extends WebTestCase {

    public function testgetWithRelatedEmail() {
        $client = static::createClient();
        $client = $client->getContainer();

        //$myParams = $client->getKernel()->getContainer()->
        Sugar::connect($client->getParameter('crmusername'),$client->getParameter('crmpassword'),$client->getParameter('crmUrl') );

        $result = Opportunity::getWithRelatedEmail("c8706224-3e03-d9d6-d03d-56ebbf20481e");
        
        // test that it return only one opportunity
        $this->assertCount(1, $result);
        $opportunity = $result[0];
              
        
        $this->assertGreaterThan(0,$opportunity->Emails, "Opportunity does not have any email");
        
        // test that the following column are returned 'name','from_addr','to_addrs','date_sent','description','description_html'
        if (count($opportunity->Emails)>0)
        {
            $email = $opportunity->Emails[0];
            //var_dump($email);
            $this->assertObjectHasAttribute('name', $email ,'Missing field name (Subject) from mail');
            $this->assertObjectHasAttribute('from_addr', $email ,'Missing field from_addr from mail');
            $this->assertObjectHasAttribute('to_addrs', $email ,'Missing field to_addrs from mail');
            $this->assertObjectHasAttribute('date_sent', $email ,'Missing field date_sent from mail');
            $this->assertObjectHasAttribute('description', $email ,'Missing field description from mail');
            $this->assertObjectHasAttribute('description_html', $email ,'Missing field description_html from mail');
                    
        }
    }

}

?>